#!/usr/bin/env perl

use utf8;
use open ':utf8';

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

use strict;

my $onlycat=shift || "";

my ($orth,$orth1,$orthothers,$caporth,$caporth2,$orth1lc,$orthotherslc,$orthlc); my ($lemma,$caplemma); my ($cat, $origcat, $tag, $cat_tag);
my %lex; my %origcats; my $o; my %interpr;

while (<>) {
  chomp;
  if (/^<orth>(.)(.*)<\/orth>/) {
    $orth1=$1; $orthothers=$2;
    $orth=$orth1.$orthothers;
    $orth1lc=lc($orth1);
    $orthotherslc=lc($orthothers);
    $orthlc=$orth1lc.$orthotherslc;
    if (++$o % 1000 == 0) {
      print STDERR "$o\r";
    }
    %interpr=();
  } elsif (/^<lex[^>]*><base>([^<]*)<\/base><ctag>([^:<]+)(?::([^<]*))?<\/ctag><\/lex>/) {
    $lemma=$1;
    $cat=$2;
    if ($onlycat ne "") {
      next if ($onlycat ne $cat);
    }
    $tag=$3;
    $origcat=$cat;
    $lex{$lemma}{$origcat}{$orth}{$cat."\t".$tag}++;
    $origcats{$origcat}++;
    if (!defined($interpr{$origcat})) {
      if (($orth1lc ne $orth1) && ($orthotherslc eq $orthothers)) {
	$lex{$lemma}{$origcat}{__uc}++;
      } elsif ($orth ne lc($orth)) {
	$lex{$lemma}{$origcat}{__ab}++;
      } else {
	$lex{$lemma}{$origcat}{__lc}++;
      }
      $lex{$lemma}{$origcat}{__all}++;
    }
    $interpr{$origcat}++;
  }
}

my @sorted_lemmas = sort keys %lex;

my %already_printed;
for my $origcat (keys %origcats) {
  %already_printed=();
  open (AF,">$origcat.af") || die "could not open $origcat.af for writing: $!\n";
  open (ALS,">$origcat.als") || die "could not open $origcat.als for writing: $!\n";
  open (AFF,">$origcat.aff") || die "could not open $origcat.aff for writing: $!\n";
  open (LOG,">$origcat.log") || die "could not open $origcat.log for writing: $!\n";
  for my $lemma (@sorted_lemmas) {
    if (defined $lex{$lemma}{$origcat}) {
      for my $orth (keys %{$lex{$lemma}{$origcat}}) {
	if ($orth !~ /^__/) {
	  for my $cat_tag (keys %{$lex{$lemma}{$origcat}{$orth}}) {
	    $cat_tag=~/^(.*)\t(.*)$/;
	    $cat=$1; $tag=$2;
	    if ($cat eq "aglt") {
	      print AF "_";
	      print AFF "_";
	    }
	    if ((100*$lex{$lemma}{$origcat}{__lc} > $lex{$lemma}{$origcat}{__all}) || $lex{$lemma}{$origcat}{__all} <= 4 || $origcat !~ /^(subst|adj)/) {
	      print AF lc($orth)."\t$lemma\t$tag\t$cat\n";
	      print AFF "$orth\t$lex{$lemma}{$origcat}{$orth}{$cat_tag}\n";
	      if (!defined($already_printed{"$lemma\t0\t\@std_$cat"})) {
		$already_printed{"$lemma\t0\t\@std_$cat"}=1;
		print ALS "$lemma\t0\t\@std_$cat\n";
	      }
	    } else {
	      if (!defined($already_printed{"$lemma\t0\t\@std_$cat"})) {
		$already_printed{"$lemma\t0\t\@std_$cat"}=0;
		print LOG "$lemma $cat rejected as common $cat (100*$lex{$lemma}{$origcat}{__lc} < $lex{$lemma}{$origcat}{__all})\n";
	      }
	    }
	    if (2*$lex{$lemma}{$origcat}{__uc} > $lex{$lemma}{$origcat}{__all} && $lex{$lemma}{$origcat}{__all} > 4 && $origcat =~ /^(subst|adj)/) {
	      $orth=~/^(.)(.*)$/;
	      $caporth=uc($1).$2;
	      $caporth2=uc($1).lc($2);
	      $lemma=~/^(.)(.*)$/;
	      $caplemma=uc($1).$2;
	      print AF "$caporth2\t$caplemma\t$tag\t$cat\n";
	      print AFF "$caporth\t$lex{$lemma}{$origcat}{$lemma}{$cat_tag}\n";
	      if (!defined($already_printed{"$caplemma\t0\t\@maj_$cat"})) {
		$already_printed{"$caplemma\t0\t\@maj_$cat"}=1;
		print ALS "$caplemma\t0\t\@maj_$cat\n";
		print LOG "$caplemma $cat\tproper name (2*$lex{$lemma}{$origcat}{__uc} > $lex{$lemma}{$origcat}{__all})\n";
	      }
	    }
	    if (2*$lex{$lemma}{$origcat}{__ab} > $lex{$lemma}{$origcat}{__all} && $lex{$lemma}{$origcat}{__all} > 4 && $origcat =~ /^(subst|adj)/) {
	      if (lc($lemma) eq lc($orth)) {
		$lemma=$orth;
		print AF "$caporth\t$caplemma\t$tag\t$cat\n";
		print AFF "$caporth\t$lex{$lemma}{$origcat}{$lemma}{$cat_tag}\n";
		if (!defined($already_printed{"$caplemma\t0\t\@maj_$cat"})) {
		  $already_printed{"$caplemma\t0\t\@maj_$cat"}=1;
		  print ALS "$caplemma\t0\t\@maj_$cat\n";
		  print LOG "$caplemma $cat\tproper name (2*$lex{$lemma}{$origcat}{__uc} > $lex{$lemma}{$origcat}{__all})\n";
		}
	      } else {
		print AF "# $orth\t$lemma\t$tag\t$cat\n";
		print AFF "# $orth\t$lex{$lemma}{$origcat}{$orth}{$cat_tag}\n";
		if (!defined($already_printed{"# $lemma\t0\t\@std_$cat"})) {
		  $already_printed{"# $lemma\t0\t\@std_$cat"}=1;
		  print ALS "# $lemma\t0\t\@std_$cat\n";
		}
	      }
	    }
	  }
	}
      }
    }
  }
  close(AF);
  close(ALS);
  close(AFF);
  close(LOG);
}
